// Implementation of gradient descent algorithm for logistic regression...

#include<bits/stdc++.h>
# define e 2.71828182846
//learning rate for gradient descent i.e Alpha
# define Alpha 0.001    
using  namespace std;


double B0=0,B1=0; 
    

//this function used to calculate a sigmoid func.
double sigmoid(double x)
{
   return 1.0/(1.0+pow(e,-x));   
}

// this function is used to calculate a costfunction of logistics regression
double costFunction(double x[],double y[],int n)
{
    double sum=0;
    for(int i=0;i<n;i++)
    {
        double h=sigmoid(B0+B1*x[i]);
        sum=sum+(y[i]*log(h))-((1-y[i])*log(1-h))); 
    }
    sum=(1/n)*sum;
    cout<<"Cost Function  is : "<<sum<<endl;
}

//this function used to train our classify model
double logisticRegressionTrain(double x[] ,double y[],int m)  //logisticRegression function for one feature and one lable                       
{
    double grad0=0,grad1=0;
    for(int i=0;i<m;i++)                                                                     
    {
        double h=sigmoid(B0+B1*x[i]);
        grad0+=-(h-y[i]);
        grad1+=-(h-y[i])*x[i];
    }
    B0=B0-(Alpha/m)*grad0;
    B1=B1-(Alpha/m)*grad1;
    cout<<"Parameter : B[0]="<<B0<<" and B[1]="<<B1<<endl;
}


//this function used to train our classify model
double logisticRegressionTest(double x[],int m)  //logisticRegression function for one feature and one lable                       
{
    double y[10000];
    for(int i=0;i<m;i++)                                                                     
    {
        y[i]=B0+B1*x[i];
        cout<<y[i]<<endl;
    }
    // final submission..
    freopen("Submission.txt","rt",stdout);                                                                       
}

//main function
int main()
{
    double x_train[10000],y_train[10000];
    
    //loading the training data from text file
    freopen("train.txt","rt",stdin);                                                                       
    for(int i=0;i<1460;i++) 
    {
        cin>>x_train[i]>>y_train[i];
    }
    //training function call
    logisticRegressionTrain(x_train,y_train,1460);
    
    //cost function call
    costFunction(x_train,y_train,1460);
    
    //loading a data set for training
    freopen("test.txt","rt",stdin);     
    double x_test[10000];          
    for(int i=0;i<1460;i++) 
    {
        cin>>x_test[i];
    }
    logisticRegressionTest(x_test,1460);
    return 0;
    
}
