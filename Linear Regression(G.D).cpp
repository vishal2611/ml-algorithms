// Implementation of gradient descent algorithm for linear regression...

#include<bits/stdc++.h>
//learning rate for gradient descent i.e Alpha
# define Alpha 0.001    
using  namespace std;


double B0=0,B1=0; 
    


// this function will calculate the  cost function for linear_regression
double costFunction(double x[],double y[],int n)
{
    double sum=0;
    for(int i=0;i<n;i++)
    {
        double h=(B0+B1*x[i]);
        sum+=pow(((h-y[i])*x[i]),2));
    }
    sum=((1/2*n)*sum);
    cout<<"Cost Function  is : "<<sum<<endl;
}

//this function used to train our classify model
double linearRegressionTrain(double x[] ,double y[],int m)  //logisticRegression function for one feature and one lable                       
{
    double grad0,grad1;
    for(int i=0;i<m;i++)                                                                     
    {
        double h=(B0+B1*x[i]);
        grad0=-(h-y[i]);
        grad1=-(h-y[i])*x[i]);
    }
    B0=B0-(Alpha/m)*grad0;
    B1=B1-(Alpha/m)*grad1;
    cout<<"Parameter : B[0]="<<B0<<" and B[1]="<<B1<<endl;
}


//this function used to train our classify model
double linearRegressionTest(double x[],int m)  //logisticRegression function for one feature and one lable                       
{
    double y[10000];
    for(int i=0;i<m;i++)                                                                     
    {
        y[i]=B0+B1*x[i];
        cout<<y[i]<<endl;
    }
    // final submission..
    freopen("Submission.txt","rt",stdout);                                                                       
}

//main function
int main()
{
    double x_train[10000],y_train[10000];
    
    //loading the training data from text file
    freopen("train.txt","rt",stdin);                                                                       
    for(int i=0;i<1460;i++) 
    {
        cin>>x_train[i]>>y_train[i];
    }
    //training function call
    linearRegressionTrain(x_train,y_train,1460);
    
    //cost function call
    costFunction(x_train,y_train,1460);
    
    //loading a data set for training
    freopen("test.txt","rt",stdin);     
    double x_test[10000];          
    for(int i=0;i<1460;i++) 
    {
        cin>>x_test[i];
    }
    linearRegressionTest(x_test,1460);
    return 0;
    
}
